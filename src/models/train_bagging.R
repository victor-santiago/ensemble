###############################################################################
# 100 executions of BAGGING
# The weak learner in use is parametrized.
###############################################################################

ROOT = getwd();

# Import utilities
source(paste(ROOT, '/src/common/utils.R', sep = ''));
source(paste(ROOT, '/src/common/weak_learners.R', sep = ''))
source(paste(ROOT, '/src/algorithms/algos.R', sep = ''));

# Install (if not present) depdencies
Utils$getPkgs(
  c('foreach',  'doParallel', 'rpart',  'pROC', 'dplyr', 'SDMTools', 'curry')
);

# Training routine.

bagging_training <- function ( DATA, weak_learner, sample_size) {
  singleton = data.frame();
  ensemble = data.frame();

  for (i in 1:sample_size) {
    print(i);
    DATA = DATA[sample(nrow(DATA)), ];
    SETS = Utils$split(DATA);

    preds     = weak_learner(SETS$train, SETS$test);
    results   = Utils$asses(SETS$test$spam, preds);
    singleton = rbind(singleton, results);

    preds     = Algo$bagging(weak_learner, SETS$train, SETS$test, 400, 2);
    results   = Utils$asses(SETS$test$spam, preds);
    ensemble = rbind(ensemble, results);
  }

  names(singleton) = c('Acc');
  names(ensemble) = c('Acc');

  return (list(
    singleton = singleton,
    ensemble = ensemble
  ))
}

# Get data
DATA = read.csv(paste(ROOT, '/data/raw/spambase.data', sep = ''));

###############################################################################
# 100 executions of BAGGING using linear regression as a weak learner
# We also train the weak learners alone to compare.
###############################################################################

output <- bagging_training(DATA, bagging_weak_learners$linear, 100)

write.csv(output$singleton, paste(ROOT, '/data/processed/bagging_linear_single.csv', sep = ''));
write.csv(output$ensemble, paste(ROOT, '/data/processed/bagging_linear_ensemble.csv', sep = ''));

###############################################################################
# 100 executions of BAGGING using linear regression as a weak learner
# We also train the weak learners alone to compare.
###############################################################################

output <- bagging_training(DATA, bagging_weak_learners$tree, 100)

write.csv(output$singleton, paste(ROOT, '/data/processed/bagging_tree_single.csv', sep = ''));
write.csv(output$ensemble, paste(ROOT, '/data/processed/bagging_tree_ensemble.csv', sep = ''));
