FROM rocker/verse

WORKDIR /home/rstudio

RUN R -e "install.packages('foreach', repos='https://cloud.r-project.org')"
RUN R -e "install.packages('dplyr', repos='https://cloud.r-project.org')"
RUN R -e "install.packages('rpart', repos='https://cloud.r-project.org')"
RUN R -e "install.packages('doParallel', repos='https://cloud.r-project.org')"
RUN R -e "install.packages('pROC', repos='https://cloud.r-project.org')"
RUN R -e "install.packages('SDMTools', repos='https://cloud.r-project.org')"
RUN R -e "install.packages('curry', repos='https://cloud.r-project.org')"
RUN R -e "install.packages('lsr', repos='https://cloud.r-project.org')"