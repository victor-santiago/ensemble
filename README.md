# Ensemble Algorithms

This project aim to study and test _Bagging_ and _Boosting__, two different machine learning techniques for building ensemble classifiers.

## Steps to replicate the analysis environment

1. Build the image in Dockerfile at the root folder:

	```$ docker build -t viktorsantiago/ensemble . ```

2. Run the image:

	```docker run --rm -e PASSWORD=pass -p 8787:8787 -v $(shell pwd):/home/rstudio viktorsantiago/ensemble:latest```


3. Open RStudio:

	Enter to ```localhost:8787``` and use _username_: "rstudio" and _password_:"pass"

Alternatively, you can just run:

```$ make start ```

This command will download and run a pre-built image from docker Hub.

## Folder Structure

This project follows a folder structure inspired by [cookie-cutter](https://drivendata.github.io/cookiecutter-data-science/):

```
.
├── data
│   ├── processed 	// Datasets containing our results
│   └── raw 		// Original dataset
├── Dockerfile
├── Makefile 		// Project's utility commands
├── notebooks 		// Explanatory notebooks
├── README.md
├── reference 		// Original dataset references & documentation
├── reports
│   ├── figures 	// Generated charts
│   └── report.pdf 	// Summary and conclusions
└── src
    ├── algorithms 	// Bagging & Boosting implementations
    ├── common 		// R utility scripts
    └── models 		// Training scripts
```

Have fun!